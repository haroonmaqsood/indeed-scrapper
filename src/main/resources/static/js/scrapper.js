function onLinkClick(id) {
	switch (id) {
	case 0:
		window.open('/', '_self');
		break;
	case 1:
		window.open('applied', '_self');
		break;
	case 2:
		window.open('failed_scrapes', '_self');
		break;
	case 3:
		window.open('interested', '_self');
		break;
	}
}

function loadApplied(id){
	$.get('/'+id, function (data){console.log(id + " loaded");});
}

function markAsApplied(id){
	var jobDTO = {
			"id" : id
		};
		$.ajax({
			type : "POST",
			url : 'markApplied',
			contentType : 'application/json',
			data : JSON.stringify(jobDTO),
			processData : false,
			success : function(data) {
				$("#row-" + id).css("visibility", "hidden");
				$("#row-" + id).css("display", "none");
			}
		});
}
function setDescription(id) {
	
	document.getElementById('id01').style.display = 'block';
	document.getElementById('jobDesc').innerHTML = (document.getElementById(id).innerHTML);
}

function removeJob(id){
	var jobDTO = {
			"id" : id
		};
		$.ajax({
			type : "POST",
			url : 'remove',
			contentType : 'application/json',
			data : JSON.stringify(jobDTO),
			processData : false,
			success : function(data) {
				$("#row-" + id).css("visibility", "hidden");
				$("#row-" + id).css("display", "none");
			}
		});
	
}

function markInterested(id) {
	var jobDTO = {
		"id" : id
	};
	$.ajax({
		type : "POST",
		url : 'interested',
		contentType : 'application/json',
		data : JSON.stringify(jobDTO),
		processData : false,
		success : function(data) {
			$("#row-" + id).css("visibility", "hidden");
			$("#row-" + id).css("display", "none");
		}
	});
}

function markNotInterested(id) {
	var jobDTO = {
		"id" : id
	};
	$.ajax({
		type : "POST",
		url : 'notinterested',
		contentType : 'application/json',
		data : JSON.stringify(jobDTO),
		processData : false,
		success : function(data) {
			$("#row-" + id).css("visibility", "hidden");
			$("#row-" + id).css("display", "none");
		}
	});
}