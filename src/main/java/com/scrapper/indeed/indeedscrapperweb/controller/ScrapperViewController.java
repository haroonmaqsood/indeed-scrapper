package com.scrapper.indeed.indeedscrapperweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.scrapper.indeed.indeedscrapperweb.dto.JobDetailDTO;
import com.scrapper.indeed.indeedscrapperweb.service.ScrapperService;

@Controller
public class ScrapperViewController {

	@Autowired
	private ScrapperService scrapperService;

	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("jobsList", scrapperService.getNewJobs());
		return "index";
	}

	@PostMapping("/notinterested")
	public String markNotInterested(@RequestBody JobDetailDTO jobDetailDTO, Model model) {
		scrapperService.markJobNotInteresting(jobDetailDTO.getId());
		model.addAttribute("jobsList", scrapperService.getNewJobs());
		return "index";
	}
	
	@PostMapping("/interested")
	public String markInterested(@RequestBody JobDetailDTO jobDetailDTO, Model model) {
		scrapperService.markInterested(jobDetailDTO.getId());
		model.addAttribute("jobsList", scrapperService.getNewJobs());
		return "index";
	}
	
	@GetMapping("/interested")
	public String getInterested(Model model) {
		model.addAttribute("interestedList", scrapperService.getInterestedJobs());
		return "interested.html";
	}

	@GetMapping("/applied")
	public String getApplied(Model model) {
		model.addAttribute("appliedJobs", scrapperService.getAppliedJobs());
		return "applied";
	}

	@GetMapping("/failed_scrapes")
	public String getFailedScrapes(Model model) {
		model.addAttribute("failedScrapes", scrapperService.getFailedScrapes());
		return "failed_scrapes";
	}

	@PostMapping("/remove")
	public String remove(@RequestBody JobDetailDTO jobDetailDTO, Model model) {
		scrapperService.removeJob(jobDetailDTO.getId());
		model.addAttribute("appliedJobs", scrapperService.getAppliedJobs());
		return "applied";
	}

	@PostMapping("/markApplied")
	public String markApplied(@RequestBody JobDetailDTO jobDetailDTO, Model model) {
		scrapperService.markJobApplied(jobDetailDTO.getId());
		model.addAttribute("jobsList", scrapperService.getNewJobs());
		return "index";
	}

	@GetMapping("/newjobs")
	public String getNewJobs(Model model) {
		model.addAttribute("jobsList", scrapperService.getNewJobs());
		return "index";
	}
}