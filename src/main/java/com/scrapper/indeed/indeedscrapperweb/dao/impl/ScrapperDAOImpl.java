package com.scrapper.indeed.indeedscrapperweb.dao.impl;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.springframework.stereotype.Repository;

import com.scrapper.indeed.indeedscrapperweb.dao.ScrapperDAO;
import com.scrapper.indeed.indeedscrapperweb.model.JobDetailModel;
import com.scrapper.indeed.indeedscrapperweb.model.ScrapeFailedModel;

@Repository
public class ScrapperDAOImpl implements ScrapperDAO {

	private static final Logger LOG = getLogger(ScrapperDAOImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public boolean insertFailedScrape(ScrapeFailedModel scrapeFailedModel) {
		boolean inserted = false;
		try {
			entityManager.persist(scrapeFailedModel);
			inserted = true;
		} catch (Exception e) {
			LOG.error("Exception {} in saving failed scrape", e.getMessage());
		}
		return inserted;
	}

	@Override
	public boolean jobExists(String url) {
		boolean exists = false;
		String queryStr = "SELECT * FROM JOBS WHERE URL = :_URL LIMIT 1";
		Query query = entityManager.createNativeQuery(queryStr, JobDetailModel.class);
		query.setParameter("_URL", url);
		try {
			query.getSingleResult();
			exists = true;
		} catch (Exception e) {
			if (!(e instanceof NoResultException))
				LOG.error("Error {} in checking job id", e.getMessage());
		}
		return exists;
	}

	@Override
	public boolean insertNewJob(JobDetailModel newJob) {
		boolean inserted = false;
		try {
			entityManager.persist(newJob);
			inserted = true;
		} catch (Exception e) {
			LOG.error("Error {} in saving new job", e.getMessage());
		}
		return inserted;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobDetailModel> getNewJobs() {
		List<JobDetailModel> retList = null;
		String queryStr = "SELECT * FROM JOBS WHERE APPLIED = 0 AND INTERESTED = 0 AND IS_ACTIVE = 1";
		Query query = entityManager.createNativeQuery(queryStr, JobDetailModel.class);
		try {
			retList = query.getResultList();
		} catch (NoResultException e) {
			LOG.error("Error {} in getting new jobs", e.getMessage());
		}
		return retList;
	}

	@Override
	public boolean updateJobField(Integer id, String field, String newVal) {
		boolean updated = false;
		String queryStr = "UPDATE JOBS SET " + field + " =  '" + newVal + "' WHERE ID = " + id;
		Query qry = entityManager.createNativeQuery(queryStr);
		try {
			qry.executeUpdate();
			updated = true;
		} catch (Exception e) {
			LOG.error("Error {} in updating field {} with value {} for jobid {}", field, newVal, id);
		}
		return updated;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobDetailModel> getAppliedJobs() {
		String queryStr = "SELECT * FROM JOBS WHERE APPLIED = 1 AND IS_ACTIVE = 1";
		Query query = entityManager.createNativeQuery(queryStr, JobDetailModel.class);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			LOG.error("Error {} in getting applied jobs", e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ScrapeFailedModel> getFailedScrapes() {
		String queryStr = "SELECT * FROM FAILED_SCRAPES WHERE IS_ACTIVE = 1";
		Query query = entityManager.createNativeQuery(queryStr, ScrapeFailedModel.class);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			LOG.error("Error {} in getting failed scrapes", e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobDetailModel> getInterestedJobs() {
		String queryStr = "SELECT * FROM JOBS WHERE INTERESTED = 1 AND APPLIED = 0 AND IS_ACTIVE = 1";
		Query query = entityManager.createNativeQuery(queryStr, JobDetailModel.class);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			LOG.error("Error {} in getInterestedJobs()", e.getMessage());
			return null;
		}
	}

	@Override
	public boolean deleteJob(int id) {
		boolean deleted = false;
		String qryString = "DELETE FROM JOBS WHERE ID = :_ID";
		Query qry = entityManager.createNativeQuery(qryString);
		qry.setParameter("_ID", id);
		try {
			qry.executeUpdate();
			deleted = true;
		} catch (Exception e) {
			LOG.error("Error {} in deleting JOB {}", e.getMessage(), id);
		}
		return deleted;
	}
}