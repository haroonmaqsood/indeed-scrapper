package com.scrapper.indeed.indeedscrapperweb.dao;

import java.util.List;

import com.scrapper.indeed.indeedscrapperweb.model.JobDetailModel;
import com.scrapper.indeed.indeedscrapperweb.model.ScrapeFailedModel;

public interface ScrapperDAO {
	boolean jobExists(String url);

	boolean insertFailedScrape(ScrapeFailedModel scrapeFailedModel);

	boolean insertNewJob(JobDetailModel newJobs);
	
	List<JobDetailModel> getNewJobs();
	
	boolean updateJobField(Integer id, String field, String newVal);
	
	boolean deleteJob(int id);
	
	List<JobDetailModel> getAppliedJobs();
	
	List<JobDetailModel> getInterestedJobs();
	
	List<ScrapeFailedModel> getFailedScrapes();
}
