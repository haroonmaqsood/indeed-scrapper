package com.scrapper.indeed.indeedscrapperweb.exception;

public class ScrapeFailedException extends Exception {
	public ScrapeFailedException() {

	}

	public ScrapeFailedException(String str) {
		super(str);
	}

	private static final long serialVersionUID = -603420751307101308L;

	private String url;
	private String stackTraceStr;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStackTraceStr() {
		return stackTraceStr;
	}

	public void setStackTraceStr(String stackTrace) {
		this.stackTraceStr = stackTrace;
	}
}
