package com.scrapper.indeed.indeedscrapperweb.model;

import static javax.persistence.GenerationType.AUTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "JOBS")
public class JobDetailModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = AUTO)
	private Integer id;

	@Column(name = "JOB_DESC" , length = 429496729)
	private String jobDesc;

	@Column(name = "URL", length = 8096)
	private String url;

	@Column(name = "APPLIED")
	private boolean applied;

	@Column(name = "REC_DATE")
	private Date recDate;

	@Column(name = "APPLIED_DATE")
	private Date appliedDate;

	@Column(name = "INTERESTED")
	private boolean interested;
	
	@Column(name = "IS_ACTIVE")
	private boolean isActive;
	
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isInterested() {
		return interested;
	}

	public void setInterested(boolean interested) {
		this.interested = interested;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isApplied() {
		return applied;
	}

	public void setApplied(boolean applied) {
		this.applied = applied;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Date getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(Date appliedDate) {
		this.appliedDate = appliedDate;
	}

}
