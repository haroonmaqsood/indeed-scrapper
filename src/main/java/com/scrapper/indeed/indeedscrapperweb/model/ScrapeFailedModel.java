package com.scrapper.indeed.indeedscrapperweb.model;

import static javax.persistence.GenerationType.AUTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "FAILED_SCRAPES")
@Table(name = "FAILED_SCRAPES")
public class ScrapeFailedModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = AUTO)
	private Integer id;

	@Column(name = "URL" , length = 8096)
	private String url;

	@Column(name = "STACK_TRACE", length = 4096)
	private String stackTrace;

	@Column(name = "FAILED_AT")
	private Date failedAt;
	
	@Column(name = "MSG")
	private String  msg;
	
	@Column(name = "IS_ACTIVE")
	private boolean isActive;

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public Date getFailedAt() {
		return failedAt;
	}

	public void setFailedAt(Date failedAt) {
		this.failedAt = failedAt;
	}
}
