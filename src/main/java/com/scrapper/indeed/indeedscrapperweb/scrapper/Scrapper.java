package com.scrapper.indeed.indeedscrapperweb.scrapper;

import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.DEF_BASE_WEBSITE;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.JOBID_LEN;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.JOB_KEY_PLACEHOLDER;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.SPACE_REPLACEMENT;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.VIEW_JOB_URL;
import static java.lang.Integer.parseInt;
import static org.jsoup.Jsoup.connect;
import static org.springframework.util.StringUtils.isEmpty;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scrapper.indeed.indeedscrapperweb.constants.Constants.Criteria;
import com.scrapper.indeed.indeedscrapperweb.dto.JobDetailDTO;
import com.scrapper.indeed.indeedscrapperweb.exception.ScrapeFailedException;

public class Scrapper {

	private List<JobDetailDTO> jobs = new LinkedList<JobDetailDTO>();
	private static final Logger LOG = LoggerFactory.getLogger(Scrapper.class);
	private List<Integer> scrappedPages = null;

	public Scrapper() {
		scrappedPages = new LinkedList<>();
	}

	public List<JobDetailDTO> getJobs() {
		return jobs;
	}

	private JobDetailDTO getJobDetails(String url) throws Exception {
		LOG.info("Getting Job Details from {}", url);
		JobDetailDTO jd = new JobDetailDTO();
		jd.setUrl(url);
		try {
			Connection connection = connect(url);
			Document jobDetailDoc = connection.get();
			if (null != jobDetailDoc) {
				Elements jobDescElement = jobDetailDoc.getElementsByClass("jobsearch-JobComponent");
				if (null != jobDescElement)
					jd.setJobDesc(jobDescElement.get(0).text());
			}
		} catch (Exception e) {
			throw e;
		}
		return jd;
	}

	public void scrape(String url, HashMap<Criteria, String> criterias) throws ScrapeFailedException {
		try {
			if (isEmpty(url))
				throw new NullPointerException("Url is Null");

			if (null != criterias)
				url = buildIndeedUrl(url, criterias);

			LOG.info("Scrapping URL {}", url);

			Connection con = connect(url);
			Document index = con.get();
			if (null == index)
				throw new NullPointerException("Index document is null");

			Elements hrefs = index.getElementsByAttribute("href");

			if (null == hrefs || hrefs.size() <= 0)
				throw new NullPointerException("No hrefs found");

			for (Element href : hrefs) {
				if (href.attr("class").trim().equals("jobtitle turnstileLink") && href.hasAttr("data-tn-element")
						&& href.attr("data-tn-element").equals("jobTitle")) {

					String jobId = href.attr("id");
					LOG.info("JobID {}", jobId);

					int jobIdLen = jobId.length();
					String urlToSave = null;
					if (jobIdLen < JOBID_LEN) {
						urlToSave = DEF_BASE_WEBSITE + href.attr("href").replaceFirst("/", "");
					} else {
						urlToSave = VIEW_JOB_URL.replaceFirst(JOB_KEY_PLACEHOLDER, jobId.substring(3, jobIdLen));
					}
					LOG.info("URL {}", urlToSave);
					jobs.add(getJobDetails(urlToSave));

				} else if (href.hasAttr("onmousedown")
						&& href.attr("onmousedown").equals("addPPUrlParam && addPPUrlParam(this);")) {
					String pageNr = href.getElementsByTag("span").html();

					if (!pageNr.matches("[0-9]+"))
						continue;

					Integer intPageNr = parseInt(pageNr);

					if (scrappedPages.contains(intPageNr))
						continue;

					scrappedPages.add(intPageNr);

					scrape(DEF_BASE_WEBSITE + href.attr("href"), null);
				}
			}
		} catch (Exception e) {
			throw buildExceptionForLogging(url, e);
		}
	}

	private ScrapeFailedException buildExceptionForLogging(String url, Exception e) {
		ScrapeFailedException sfe = new ScrapeFailedException();
		sfe.setUrl(url);
		StringBuilder sb = new StringBuilder();

		StackTraceElement steArr[] = e.getStackTrace();
		if (null != steArr) {
			for (StackTraceElement ste : steArr)
				sb.append("\n").append(ste.toString());
		}
		sfe.setStackTraceStr(sb.toString());

		return sfe;
	}

	private String escapeSpaces(String from, String replacement) {
		return from.replaceAll("\\s", replacement);
	}

	private String buildIndeedUrl(String from, HashMap<Criteria, String> criterias) {
		LOG.info("Criterias {}", criterias.toString());
		StringBuilder sb = new StringBuilder(from);
		criterias.entrySet().forEach((criteria) -> {
			sb.append(escapeSpaces(criteria.getKey().getmKey(), SPACE_REPLACEMENT)).append("=")
					.append(escapeSpaces(criteria.getValue(), SPACE_REPLACEMENT)).append("&");
		});

		String retStr = sb.toString();
		return retStr.substring(0, retStr.length() - 1);
	}
}
