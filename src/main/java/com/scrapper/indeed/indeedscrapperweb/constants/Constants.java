package com.scrapper.indeed.indeedscrapperweb.constants;

public interface Constants {

	String DEF_BASE_WEBSITE = "https://www.indeed.ae/";
	String DEF_BASE_URL = DEF_BASE_WEBSITE + "jobs?";
	String SPACE_REPLACEMENT = "+";
	String JOB_KEY_PLACEHOLDER = "JK";
	String VIEW_JOB_URL = DEF_BASE_WEBSITE + "viewjob?&jk=" + JOB_KEY_PLACEHOLDER + "&vjs=3";
	int JOBID_LEN = 19;

	enum Criteria {
		LOCATION("l"), JOBTYPE("jt"), SORT("sort"), QUERY("q");

		Criteria(String key) {
			mKey = key;
		}

		public String getmKey() {
			return mKey;
		}

		private String mKey;
	}
}
