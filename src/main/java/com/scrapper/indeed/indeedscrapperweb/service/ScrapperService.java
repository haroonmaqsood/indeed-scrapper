package com.scrapper.indeed.indeedscrapperweb.service;

import java.util.List;

import com.scrapper.indeed.indeedscrapperweb.dto.JobDetailDTO;
import com.scrapper.indeed.indeedscrapperweb.dto.ScrapeFailedDTO;
import com.scrapper.indeed.indeedscrapperweb.exception.ScrapeFailedException;

public interface ScrapperService {
	boolean insertFailedScrape(ScrapeFailedException exception);

	boolean insertJobs(List<JobDetailDTO> scrappedJobs);

	List<JobDetailDTO> getNewJobs();

	boolean markJobNotInteresting(Integer id);

	boolean markJobApplied(Integer id);

	List<JobDetailDTO> getAppliedJobs();
	
	List<JobDetailDTO> getInterestedJobs();

	boolean removeJob(int id);
	
	List<ScrapeFailedDTO> getFailedScrapes();
	
	boolean markInterested(Integer id);
	
	
}
