package com.scrapper.indeed.indeedscrapperweb.service.impl;

import static java.time.Instant.now;
import static java.util.Date.from;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.util.CollectionUtils.isEmpty;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scrapper.indeed.indeedscrapperweb.dao.ScrapperDAO;
import com.scrapper.indeed.indeedscrapperweb.dto.JobDetailDTO;
import com.scrapper.indeed.indeedscrapperweb.dto.ScrapeFailedDTO;
import com.scrapper.indeed.indeedscrapperweb.exception.ScrapeFailedException;
import com.scrapper.indeed.indeedscrapperweb.model.JobDetailModel;
import com.scrapper.indeed.indeedscrapperweb.model.ScrapeFailedModel;
import com.scrapper.indeed.indeedscrapperweb.service.ScrapperService;

@Service
public class ScrapperServiceImpl implements ScrapperService {

	private static final Logger LOG = getLogger(ScrapperServiceImpl.class);

	@Autowired
	private ScrapperDAO scrapperDAO;

	private List<JobDetailDTO> toJobDetailDtoLst(List<JobDetailModel> jobDetailModelLst) {
		List<JobDetailDTO> newJobsDTOs = null;
		if (!isEmpty(jobDetailModelLst)) {
			newJobsDTOs = new LinkedList<JobDetailDTO>();
			for (JobDetailModel newJob : jobDetailModelLst) {
				JobDetailDTO jobDetail = new JobDetailDTO();
				copyProperties(newJob, jobDetail);
				newJobsDTOs.add(jobDetail);
			}
		}
		return newJobsDTOs;
	}

	@Transactional(propagation = REQUIRED)
	@Override
	public boolean insertFailedScrape(ScrapeFailedException exception) {
		ScrapeFailedModel model = new ScrapeFailedModel();
		model.setFailedAt(Date.from(Instant.now()));
		model.setStackTrace(exception.getStackTraceStr());
		model.setUrl(exception.getUrl());
		model.setMsg(exception.getMessage());
		model.setActive(true);
		return scrapperDAO.insertFailedScrape(model);
	}

	@Transactional(propagation = REQUIRED)
	@Override
	public boolean insertJobs(List<JobDetailDTO> scrappedJobs) {
		boolean inserted = false;
		for (JobDetailDTO job : scrappedJobs) {
			String jobUrl = job.getUrl();
			if (scrapperDAO.jobExists(jobUrl)) {
				LOG.warn("Job {} already exists, skipping it", jobUrl);
			} else {
				JobDetailModel model = new JobDetailModel();
				model.setApplied(false);
				model.setJobDesc(job.getJobDesc());
				model.setRecDate(from(now()));
				model.setUrl(job.getUrl());
				model.setInterested(false);
				model.setActive(true);
				inserted = scrapperDAO.insertNewJob(model);
			}
		}
		return inserted;
	}

	
	@Transactional(readOnly = true)
	@Override
	public List<JobDetailDTO> getNewJobs() {
		return toJobDetailDtoLst(scrapperDAO.getNewJobs());
	}

	@Transactional(propagation = REQUIRED)
	@Override
	public boolean markJobNotInteresting(Integer id) {
		return scrapperDAO.updateJobField(id, "IS_ACTIVE", "0");
	}

	@Transactional(propagation = REQUIRED)
	@Override
	public boolean markJobApplied(Integer id) {
		return scrapperDAO.updateJobField(id, "APPLIED", "1")
				&& scrapperDAO.updateJobField(id, "APPLIED_DATE", new Timestamp(from(now()).getTime()).toString());

	}

	@Transactional(readOnly = true)
	@Override
	public List<JobDetailDTO> getAppliedJobs() {
		return toJobDetailDtoLst(scrapperDAO.getAppliedJobs());
	}

	@Transactional(propagation = REQUIRED)
	public boolean markInterested(Integer id) {
		return scrapperDAO.updateJobField(id, "INTERESTED", "1");
	}

	@Transactional(propagation = REQUIRED)
	@Override
	public boolean removeJob(int id) {
		return scrapperDAO.updateJobField(id, "IS_ACTIVE", "0");
	}

	@Transactional(readOnly = true)
	@Override
	public List<ScrapeFailedDTO> getFailedScrapes() {
		List<ScrapeFailedModel> failedScrapesModels = scrapperDAO.getFailedScrapes();
		List<ScrapeFailedDTO> failedScrapesDTOS = null;
		if (!isEmpty(failedScrapesModels)) {
			failedScrapesDTOS = new LinkedList<>();
			for (ScrapeFailedModel failedScrape : failedScrapesModels) {
				ScrapeFailedDTO dto = new ScrapeFailedDTO();
				copyProperties(failedScrape, dto);
				failedScrapesDTOS.add(dto);
			}
		}
		return failedScrapesDTOS;
	}

	@Transactional(readOnly = true)
	@Override
	public List<JobDetailDTO> getInterestedJobs() {
		return toJobDetailDtoLst(scrapperDAO.getInterestedJobs());
	}
}
