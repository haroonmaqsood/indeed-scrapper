package com.scrapper.indeed.indeedscrapperweb.scheduler;

import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.DEF_BASE_URL;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.Criteria.JOBTYPE;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.Criteria.LOCATION;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.Criteria.QUERY;
import static com.scrapper.indeed.indeedscrapperweb.constants.Constants.Criteria.SORT;
import static java.time.Instant.now;
import static java.util.Date.from;
import static org.springframework.util.StringUtils.isEmpty;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.scrapper.indeed.indeedscrapperweb.constants.Constants.Criteria;
import com.scrapper.indeed.indeedscrapperweb.dto.JobDetailDTO;
import com.scrapper.indeed.indeedscrapperweb.exception.ScrapeFailedException;
import com.scrapper.indeed.indeedscrapperweb.scrapper.Scrapper;
import com.scrapper.indeed.indeedscrapperweb.service.ScrapperService;

@Component
public class ScrapperScheduler {

	private static final Logger LOG = LoggerFactory.getLogger(ScrapperScheduler.class);

	@Autowired
	private ScrapperService scrapperService;

	@Value("${job.type}")
	private String jobType;

	@Value("${location}")
	private String location;

	@Value("${sort}")
	private String sort;

	@Value("${query}")
	private String query;

	@Scheduled(fixedDelayString = "${scheduler.delay}")
	public void scrapeIndeed() {
		LOG.info("Starting...");
		Scrapper scrapper = new Scrapper();

		try {
			HashMap<Criteria, String> criteria = new HashMap<Criteria, String>();

			if (isEmpty(query))
				throw new ScrapeFailedException("query  is null");

			criteria.put(QUERY, query);

			if (!isEmpty(location))
				criteria.put(LOCATION, location);

			if (!isEmpty(sort))
				criteria.put(SORT, sort);

			if (!isEmpty(jobType))
				criteria.put(JOBTYPE, jobType);

			scrapper.scrape(DEF_BASE_URL, criteria);

			List<JobDetailDTO> jobs = scrapper.getJobs();
			LOG.info("Found {} Jobs", jobs.size());
			scrapperService.insertJobs(jobs);

		} catch (ScrapeFailedException e) {
			LOG.warn("Failed Scrape at  {} logging to failed Scrapes", from(now()));
			scrapperService.insertFailedScrape(e);
		}
		LOG.info("Finished...");
	}
}
