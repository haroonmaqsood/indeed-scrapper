package com.scrapper.indeed.indeedscrapperweb;

import static org.springframework.boot.SpringApplication.run;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class IndeedScrapperWebApplication {
	public static void main(String[] args) {
		run(IndeedScrapperWebApplication.class, args);
	}
}