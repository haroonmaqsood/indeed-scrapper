package com.scrapper.indeed.indeedscrapperweb.dto;

import java.util.Date;

public class ScrapeFailedDTO {

	private Integer id;

	private String url;

	private String stackTrace;

	private Date failedAt;
	
	private String  msg;
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public Date getFailedAt() {
		return failedAt;
	}

	public void setFailedAt(Date failedAt) {
		this.failedAt = failedAt;
	}
}
